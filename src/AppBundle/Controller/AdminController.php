<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sunra\PhpSimple\HtmlDomParser;
use Application\Sonata\MediaBundle\Entity\Media;

use AppBundle\Form\RecipeType;
use AppBundle\Form\EquipmentType;
use AppBundle\Form\IngredientType;
use AppBundle\Form\IngredientSearchType;
use AppBundle\Form\ImportUrlType;

use AppBundle\Entity\Recipe;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\Step;
use AppBundle\Entity\Equipment;
use AppBundle\Entity\UserHasHistory;
use AppBundle\Entity\UserHasFavorite;

class AdminController extends Controller
{
  /**
  * @Route("/admin")
  */
  public function dashboardAction(Request $request)
  {
    return $this->render('admin/dashboard.html.twig', array());
  }

  /**
  * @Route("/admin/recipes")
  */
  public function recipesAction(Request $request)
  {
    $entityManager = $this->getDoctrine()->getManager();

    $recipes = $entityManager->getRepository("AppBundle:Recipe")->findBy(array(), array("id" => "DESC"));
    return $this->render('admin/recipes/list.html.twig', array(
      'recipes' => $recipes
    ));
  }

  /**
  * @Route("/admin/recipes/new")
  */
  public function recipesNewAction(Request $request)
  {
    $recipe = new Recipe();
    $form = $this->createForm(RecipeType::class, $recipe);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $recipe = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($recipe);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_recipes');
    }
    return $this->render('admin/recipes/new.html.twig', array("form" => $form->createView()));
  }

  /**
  * @Route("/admin/recipes/{id}/edit")
  */
  public function recipesEditAction(Request $request, Recipe $recipe)
  {
    $form = $this->createForm(RecipeType::class, $recipe);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $recipe = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();

      // Upload image
      if ($form["base64"]->getData()) {
        $media = $this->upload_image($form["base64"]->getData());
        $recipe->setMedia($media);
      }

      $entityManager->persist($recipe);
      $entityManager->flush();
      // return $this->redirectToRoute('app_admin_recipes');
    }
    return $this->render('admin/recipes/edit.html.twig', array(
      "form" => $form->createView(),
      "recipe" => $recipe
    ));
  }

  /**
  * @Route("/admin/recipes/{id}/delete")
  */
  public function recipesDeleteAction(Request $request, Recipe $recipe)
  {
      $em = $this->getDoctrine()->getManager();

      $userHistory = $em->getRepository(UserHasHistory::class)->findBy(array("recipe" => $recipe));
      $userFavorite = $em->getRepository(UserHasFavorite::class)->findBy(array("recipe" => $recipe));

      foreach ($userHistory as $uh) {
          $em->remove($uh);
      }
      foreach ($userFavorite as $uf) {
        $em->remove($uf);
      }
      $em->remove($recipe);
      $em->flush();
      return $this->redirectToRoute('app_admin_recipes');
  }

  /**
  * @Route("/admin/ingredient")
  */
  public function ingredientAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();

    $form = $this->createForm(IngredientSearchType::class);
    $form->handleRequest($request);

    $whereRequest = "";
    if ($form->isSubmitted() && $form->isValid()) {
      $whereRequest = " WHERE i.name LIKE '%" . $form->getData()['name'] . "%'";
    }

    $dql = "SELECT i FROM AppBundle:Ingredient i" . $whereRequest . " ORDER BY i.name ASC";

    $query = $em->createQuery($dql);
    $paginator  = $this->get('knp_paginator');
    $pagination = $paginator->paginate(
      $query, /* query NOT result */
      $request->query->getInt('page', 1) /* page number */,
      20 /* limit per page */
    );

    return $this->render('admin/ingredient/list.html.twig', array(
      'ingredients' => $pagination,
      'form' => $form->createView()
    ));
  }

  /**
  * @Route("/admin/ingredient/new")
  */
  public function ingredientNewAction(Request $request)
  {
    $recipe = new Ingredient();
    $form = $this->createForm(IngredientType::class, $recipe);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $recipe = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($recipe);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_ingredient');
    }
    return $this->render('admin/ingredient/new.html.twig', array("form" => $form->createView()));
  }

  /**
  * @Route("/admin/ingredient/{id}/edit")
  */
  public function ingredientEditAction(Request $request, Ingredient $ingredient)
  {
    $form = $this->createForm(IngredientType::class, $ingredient);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $ingredient = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($ingredient);
      $entityManager->flush();
      // return $this->redirectToRoute('app_admin_ingredient');
    }
    return $this->render('admin/ingredient/edit.html.twig', array("form" => $form->createView()));
  }

  /**
  * @Route("/admin/equipment/new")
  */
  public function equipmentNewAction(Request $request)
  {
    $equipment = new Equipment();
    $form = $this->createForm(EquipmentType::class, $equipment);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $recipe = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($equipment);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_equipment');
    }
    return $this->render('admin/equipment/new.html.twig', array("form" => $form->createView()));
  }

  /**
  * @Route("/admin/equipment")
  */
  public function equipmentAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();

    $equipments = $em->getRepository(Equipment::class)->findAll();

    return $this->render('admin/equipment/list.html.twig', array(
      'equipments' => $equipments
    ));
  }

  /**
  * @Route("/admin/equipment/{id}/edit")
  */
  public function equipmentEditAction(Request $request, Equipment $equipment)
  {
    $form = $this->createForm(EquipmentType::class, $equipment);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $equipment = $form->getData();
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($equipment);
      $entityManager->flush();
      // return $this->redirectToRoute('app_admin_ingredient');
    }
    return $this->render('admin/equipment/edit.html.twig', array("form" => $form->createView()));
  }

  /**
  * @Route("/admin/recipe/import/marmiton")
  */
  public function importMarmitonAction(Request $request)
  {
    $form = $this->createForm(ImportUrlType::class);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $url = $form->getData()["url"];
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_HEADER, 0);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($curl, CURLOPT_URL, $url);
      $html = curl_exec($curl);

      $recipe = new Recipe();

      $dom = HtmlDomParser::str_get_html($html);
      $recipe->setName($dom->find("h1.main-title")[0]->plaintext);
      $recipe->setPreparationTime(preg_replace("/[^0-9,.]/", "", $dom->find("div.recipe-infos__timmings__preparation")[0]->find("span.recipe-infos__timmings__value")[0]));
      if (count($dom->find("div.recipe-infos__timmings__cooking")) > 0)
      $recipe->setCookTime(preg_replace("/[^0-9,.]/", "", $dom->find("div.recipe-infos__timmings__cooking")[0]->find("span.recipe-infos__timmings__value")[0]));
      foreach ($dom->find("li.recipe-preparation__list__item") as $key => $step_item) {
        $step = new Step();
        $step->setText(trim(preg_replace('/\s+/', ' ', str_replace($step_item->find('h3')[0]->plaintext, "", $step_item->plaintext))));
        $step->setPosition($key);
        $recipe->addStep($step);
      }
      $description = "Image : " . $dom->find('img#af-diapo-desktop-0_img')[0]->src . "\n";

      foreach ($dom->find("li.recipe-ingredients__list__item") as $ingredient_item) {
        $description .= trim(preg_replace('/\s+/', ' ', $ingredient_item->plaintext)) . "\n";
      }
      $recipe->setDescription($description);
      if (count($dom->find('span.recipe-reviews-list__review__head__infos__rating__value')) > 0)
      $recipe->setNote(trim($dom->find('span.recipe-reviews-list__review__head__infos__rating__value')[0]->plaintext));
      $recipe->setForPerson(trim($dom->find('span.recipe-infos__quantity__value')[0]->plaintext));
      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($recipe);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_recipesedit', array("id" => $recipe->getId()));
    }
    return $this->render('admin/import.html.twig', array(
      "form" => $form->createView()
    ));
  }

  /**
  * @Route("/admin/recipe/import/750g")
  */
  public function import750gAction(Request $request)
  {
    $form = $this->createForm(ImportUrlType::class);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $url = $form->getData()["url"];
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_HEADER, 0);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($curl, CURLOPT_URL, $url);
      $html = curl_exec($curl);

      $recipe = new Recipe();

      $dom = HtmlDomParser::str_get_html($html);
      $recipe->setName($dom->find("h1.u-title-page")[0]->plaintext);

      foreach ($dom->find("div.c-recipe-steps__item") as $key => $step_item) {
        $step = new Step();
        $step->setText(trim(preg_replace('/\s+/', ' ', $step_item->plaintext)));
        $step->setPosition($key);
        $recipe->addStep($step);
      }

      $description = "Image : " . $dom->find('picture.c-swiper__media')[0]->find("img")[0]->src . "\n";

      foreach ($dom->find("li.ingredient") as $ingredient_item) {
        $description .= trim(preg_replace('/\s+/', ' ', $ingredient_item->plaintext)) . "\n";
      }
      $recipe->setDescription($description);

      if (count($dom->find('li[title=Temps de préparation]')) > 0)
      $recipe->setPreparationTime(preg_replace("/[^0-9,.]/", "", $dom->find('li[title=Temps de préparation]')[0]->plaintext));

      if (count($dom->find('li[title=Temps de cuisson]')) > 0)
      $recipe->setCookTime(preg_replace("/[^0-9,.]/", "", $dom->find('li[title=Temps de cuisson]')[0]->plaintext));

      if (count($dom->find('li[title=Temps d\'attente]')) > 0)
      $recipe->setRestTime(preg_replace("/[^0-9,.]/", "", $dom->find('li[title=Temps d\'attente]')[0]->plaintext));

      $recipe->setForPerson(preg_replace("/[^0-9,.]/", "", $dom->find('h2.u-title-section')[0]->plaintext));

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($recipe);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_recipesedit', array("id" => $recipe->getId()));
    }
    return $this->render('admin/import.html.twig', array(
      "form" => $form->createView()
    ));
  }

  /**
  * @Route("/admin/recipe/import/cuisineaz")
  */
  public function importCuisineAZAction(Request $request)
  {
    $form = $this->createForm(ImportUrlType::class);
    $form->handleRequest($request);
    if ($form->isSubmitted() && $form->isValid()) {
      $url = $form->getData()["url"];
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_HEADER, 0);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
      curl_setopt($curl, CURLOPT_URL, $url);
      $html = curl_exec($curl);

      $recipe = new Recipe();

      $dom = HtmlDomParser::str_get_html($html);

      $recipe->setName($dom->find("h1")[0]->plaintext);

      if (count($dom->find("span#ContentPlaceHolder_LblRecetteNombre")) > 0)
      $recipe->setForPerson(preg_replace("/[^0-9,.]/", "", $dom->find("span#ContentPlaceHolder_LblRecetteNombre")[0]->plaintext));
      if (count($dom->find("span#ContentPlaceHolder_LblRecetteTempsPrepa")) > 0)
      $recipe->setPreparationTime(preg_replace("/[^0-9,.]/", "", $dom->find("span#ContentPlaceHolder_LblRecetteTempsPrepa")[0]->plaintext));
      if (count($dom->find("span#ContentPlaceHolder_LblRecetteTempsCuisson")) > 0)
      $recipe->setCookTime(preg_replace("/[^0-9,.]/", "", $dom->find("span#ContentPlaceHolder_LblRecetteTempsCuisson")[0]->plaintext));
      if (count($dom->find("span#ContentPlaceHolder_LblRecetteTempsRepos")) > 0)
      $recipe->setRestTime(preg_replace("/[^0-9,.]/", "", $dom->find("span#ContentPlaceHolder_LblRecetteTempsRepos")[0]->plaintext));



      $dom_preparation = $dom->find('div#preparation')[0];
      foreach ($dom_preparation->find("p.p10") as $key => $step_item) {
        $step = new Step();
        $step->setText(trim(preg_replace('/\s+/', ' ', str_replace($step_item->find('span')[0]->plaintext, "", $step_item->plaintext))));
        $step->setPosition($key);
        $recipe->addStep($step);
      }
      $description = "Image : " . $dom->find('img#ContentPlaceHolder_recipeImgLarge')[0]->src . "\n";

      $dom_preparation = $dom->find('div#preparation')[0];

      $dom_ingredient = $dom->find('section.recipe_ingredients')[0];
      foreach ($dom_ingredient->find("li") as $ingredient_item) {
        $description .= trim(preg_replace('/\s+/', ' ', $ingredient_item->plaintext)) . "\n";
      }
      $recipe->setDescription($description);

      $entityManager = $this->getDoctrine()->getManager();
      $entityManager->persist($recipe);
      $entityManager->flush();
      return $this->redirectToRoute('app_admin_recipesedit', array("id" => $recipe->getId()));
    }
    return $this->render('admin/import.html.twig', array(
      "form" => $form->createView()
    ));
  }

  private function upload_image($img) {
    $mediaManager = $this->container->get('sonata.media.manager.media');

    $dataImg = explode(',', $img);
    $name = 'uploads/tmp/'.uniqid().'.png';
    file_put_contents($name, base64_decode($dataImg[1]));

    $media = new Media();
    $media->setBinaryContent($name);
    $media->setContext('default');
    $media->setProviderName('sonata.media.provider.image');

    $mediaManager->save($media);
    unlink($name);
    return ($media);
  }
}
