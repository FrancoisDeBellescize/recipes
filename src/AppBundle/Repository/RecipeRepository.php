<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

use AppBundle\Entity\PartHasIngredient;
use AppBundle\Entity\RecipeHasPart;
use AppBundle\Entity\Equipment;
use AppBundle\Entity\Recipe;

class RecipeRepository extends EntityRepository
{
  public function getLast($limit = 3){
    $qb = $this->createQueryBuilder('r');
    $qb->select('r')
    ->orderBy('r.created_at', 'DESC')
    ->setMaxResults($limit);

    return $qb->getQuery()->getResult();
  }

  public function findByIngredients($form, $getQuery = false){

    $query = $this->createQueryBuilder('recipe');

    // Equipments
    $equipments = array();
    foreach($form["equipments"]->getData() as $key => $tmp) {
      $equipments[] = $tmp->getId();
    }
    if (count($equipments) > 0) {
      $query->leftJoin('recipe.equipments', 'equipment');
      $query->andWhere($query->expr()->in('equipment.id', $equipments));
    }

    // Categories
    $categories = array();
    foreach($form["categories"]->getData() as $key => $tmp) {
      $categories[] = $tmp->getId();
    }
    if (count($categories) > 0) {
      $query->leftJoin('recipe.category', 'cat');
      $query->andWhere($query->expr()->in('cat.id', $categories));
    }

    // Ingredients
    $ingredients = array();
    foreach($form["ingredients"]->getData() as $key => $tmp) {
      $ingredients[] = $tmp["id"];
    }
    if (count($ingredients) > 0) {
      $query->leftJoin('recipe.parts', 'part');
      $query->leftJoin('part.ingredients', 'parhasingredient');
      $query->leftJoin('parhasingredient.ingredient', 'ingredient');
      $query->andWhere($query->expr()->in('ingredient.id', $ingredients));
    }

    // Name
    $query->andWhere("recipe.name LIKE '%" . $form["name"]->getData() . "%'");

    //
    // $query = $this->createQueryBuilder('g')
    // ->innerJoin(RecipeHasPart::class, 'rhp', Join::WITH, 'rhp.recipe = g.id');
    //
    // // Get Categories in array
    // $categories = [];
    // foreach($form["categories"]->getData() as $category) {
    //   $categories[] = $category->getId();
    // }
    // if (count($categories) > 0) {
    //   $query->andWhere('g.category IN (:categories)')
    //   ->setParameter('categories', $categories);
    // }
    //
    // foreach($form["equipments"]->getData() as $key => $tmp){
    //   $query->innerJoin('recipe_equipment', 'e' . $key , Join::WITH, 'g.id = e' . $key . '.recipe')
    //   ->andWhere('e' . $key . '.id = :ingredient')
    //   ->setParameter('ingredient', 1);
    // }


    // foreach($form["ingredients"]->getData() as $key => $tmp){
    //   $query->innerJoin(PartHasIngredient::class, 'phi' . $key , Join::WITH, 'rhp.id = phi' . $key . '.part')
    //   ->andWhere('phi' . $key . '.ingredient = :ingredient')
    //   ->setParameter('ingredient', $tmp["id"]);
    // }
    // die($query->getQuery()->getSql());
    if ($getQuery)
    return $query->getQuery();

    return $query->getQuery()->getResult();
  }
}
