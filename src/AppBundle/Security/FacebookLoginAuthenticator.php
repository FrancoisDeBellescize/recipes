<?php
namespace AppBundle\Security;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;

class FacebookLoginAuthenticator implements OAuthAwareUserProviderInterface
{
  private $em;

  public function __construct(EntityManager $em, UserManager $userProvider)
  {
    $this->em = $em;
  }

  /**
  * {@inheritdoc}
  */
  public function loadUserByOAuthUserResponse(UserResponseInterface $response)
  {
    if(!$user = $this->em->getRepository("AppBundle:User")->findOneByFacebook($response->getUsername())) {
      if(($user = $this->em->getRepository("AppBundle:User")->findOneByEmail($response->getEmail()))) {
        $user->setFacebook($response->getUsername()); // Facebook Id
      } else {
        $user = new User();
        $user->setUsername($response->getEmail());
        $user->setEmail($response->getEmail());
        $user->setFirstname($response->getFirstName());
        $user->setLastname($response->getLastName());
        $user->setPassword('');
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());
        $user->setFacebook($response->getUsername()); // Facebook Id
        $this->em->persist($user);
      }
      $this->em->flush();
      return $user;
    }
  }
}
