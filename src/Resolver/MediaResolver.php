<?php
namespace Resolver;

use Application\Sonata\MediaBundle\Entity\Media as Media;
use Doctrine\ORM\EntityManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\HttpFoundation\RequestStack;

class MediaResolver implements ResolverInterface
{
  /**
  * @var EntityManagerInterface
  */
  private $entityManager;

  /**
  * @var Container
  */
  private $container;

  /**
  * @var RequestStack
  */
  private $requestStack;

  public function __construct(EntityManagerInterface $entityManagerInterface, Container $container, RequestStack $requestStack)
  {
    $this->entityManager = $entityManagerInterface;
    $this->container = $container;
    $this->requestStack = $requestStack;
  }

  public function resolveMedia(Media $media)
  {
    $provider = $this->container->get($media->getProviderName());
    return $this->requestStack->getCurrentRequest()->getHost() . $provider->generatePublicUrl($media, 'default_big');
  }

}
