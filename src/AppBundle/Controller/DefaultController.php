<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use AppBundle\Entity\Recipe;
use AppBundle\Entity\Ingredient;
use AppBundle\Entity\UserHasFavorite;
use AppBundle\Entity\UserHasHistory;

use AppBundle\Form\AdvanceSearchType;

class DefaultController extends Controller
{
  /**
  * @Route("/", name="homepage")
  */
  public function indexAction(Request $request)
  {
    $repository = $this->getDoctrine()->getRepository('AppBundle:Recipe');
    $recipes = $repository->getLast();

    return $this->render('default/index.html.twig', array("recipes" => $recipes));
  }

  /**
  * @Route("/dashboard", name="dashboard")
  */
  public function dashboardAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();
    $user = $this->getUser();

    $favorites = $em->getRepository(UserHasFavorite::class)->findBy(array(
      "user" => $user
    ), array(
      "created" => "DESC"
    ),
    4);

    $histories = $em->getRepository(UserHasHistory::class)->findBy(array(
      "user" => $user
    ), array(
      "created" => "DESC"
    ),
    4);

    return $this->render('default/dashboard.html.twig', array(
      "favorites" => $favorites,
      "histories" => $histories
    ));
  }

  /**
  * @Route("/recipe_to_favorite/{id}")
  */
  public function recipeToFavoriteAction(Request $request, Recipe $recipe)
  {
    $user = $this->getUser();
    if (!$user) {
      return new JsonResponse(array(
        'error' => 'Vous devez être connécté'
      ));
    }

    $em = $this->getDoctrine()->getManager();
    $userHasFavorite = $em->getRepository(UserHasFavorite::class)->findOneBy(array(
      "user" => $user,
      "recipe" => $recipe
    ));

    $response = "";
    if ($userHasFavorite){
      $em->remove($userHasFavorite);
      $response = "remove";
    } else {
      $userHasFavorite = new UserHasFavorite();
      $userHasFavorite->setRecipe($recipe);
      $userHasFavorite->setUser($user);

      $user->addFavorite($userHasFavorite);
      $em->persist($user);
      $response = "add";
    }

    $em->flush();
    return new JsonResponse(array(
      'action' => $response
    ));
  }

  /**
  * @Route("/advance_search", name="advance_search")
  */
  public function advanceSearchAction(Request $request)
  {
    $form = $this->createForm(AdvanceSearchType::class);
    $form->handleRequest($request);
    $em = $this->getDoctrine()->getManager();
    $searchResult = null;
    if ($form->isSubmitted() && $form->isValid()) {
      $searchResult = $em->getRepository(Recipe::class)->findByIngredients($form);
    }

    $ingredients = $em->getRepository(Ingredient::class)->findAll();

    return $this->render('default/advance_search.html.twig', array(
      'form' => $form->createView(),
      'ingredients' => $ingredients,
      'result' => $searchResult
    ));
  }

  /**
  * @Route("/advance_search/ingredients", name="advance_search_ingredients")
  */
  public function advanceSearchIngredientsAction(Request $request)
  {
    $em = $this->getDoctrine()->getManager();

    $repo = $em->getRepository(Ingredient::class);
    $ingredients = $repo->createQueryBuilder('a')
    ->where('a.name LIKE :name')
    ->setParameter('name', '%'.$request->query->get('name').'%')
    ->getQuery()->getResult();


    $result = [];
    foreach ($ingredients as $ingredient) {
      $new_ingredient = array();
      $new_ingredient["id"] = $ingredient->getId();
      $new_ingredient["name"] = $ingredient->getName();
      $result[] = $new_ingredient;
    }

    return new JsonResponse($result);
  }
}
