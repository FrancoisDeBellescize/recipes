<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Unit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UnitFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    $units = array(
      ["name" => "Gramme", "symbol" => "g"],
      ["name" => "Litre", "symbol" => "L"],
      ["name" => "Millilitre", "symbol" => "ml"],
      ["name" => "Cuillère à café", "symbol" => null],
      ["name" => "Cuillère à soupe", "symbol" => null],
      ["name" => "Unité", "symbol" => null],
      ["name" => "Ounce", "symbol" => "oz"],
      ["name" => "Pound", "symbol" => "lb"],
      ["name" => "Sachet", "symbol" => null],
      ["name" => "Pincée", "symbol" => null],
      ["name" => "Cup", "symbol" => "cup"]
    );

    foreach ($units as $key => $unit) {
      $object = new Unit();
      $object->setName($unit["name"]);
      $object->setSymbol($unit["symbol"]);
      $manager->persist($object);
    }

    $manager->flush();
  }
}
