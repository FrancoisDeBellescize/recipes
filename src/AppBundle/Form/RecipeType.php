<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Valid;
use AppBundle\Form\RecipeHasIngredientType;
use AppBundle\Form\RecipeHasPartType;
use AppBundle\Form\StepType;
use AppBundle\Entity\Recipe;
use AppBundle\Entity\Step;

class RecipeType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', null, array(
      'label' => "Titre de la recette"
    ))
    ->add('description', null, array(
      'attr' => array(
        'class' => 'materialize-textarea'
      )
    ))
    ->add('base64', HiddenType::class, array(
      'mapped' => false
    ))
    ->add('category', null, array(
      'label' => 'Categorie'
    ))
    ->add('parts', CollectionType::class, [
      'entry_type'   => RecipeHasPartType::class,
      'label'        => false,
      'allow_add'    => true,
      'allow_delete' => true,
      'prototype'    => true,
      'required'     => false,
      'by_reference' => false,
      'constraints' => array(new Valid()),
      'attr'         => [
        'class' => "parts-collection",
      ],
    ])
    ->add('steps', CollectionType::class, [
      'entry_type'   => StepType::class,
      'label'        => false,
      'allow_add'    => true,
      'allow_delete' => true,
      'prototype'    => true,
      'required'     => false,
      'by_reference' => false,
      'attr'         => [
        'class' => "steps-collection",
      ],
    ])
    // ->add('parts', null, array(
    //   'label' => 'Parties'
    // ))
    ->add('cookTime', null, array(
      'label' => 'Temps de cuisson'
    ))
    ->add('preparationTime', null, array(
      'label' => 'Temps de préparation'
    ))
    ->add('restTime', null, array(
      'label' => 'Temps de repos'
    ))
    ->add('forPerson', null, array(
      'label' => 'Cette recette est pour X personnes'
    ))
    // ->add('media', null, array(
    //   'label' => 'Image'
    // ))
    ->add('note', null, array(
      'label' => 'Note'
    ))
    ->add('equipments')
    ->add('submit', SubmitType::class, array(
      'label' => 'Enregistrer',
      'attr' => array(
        'class' => 'btn'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults(array(
      'data_class' => Recipe::class,
      'cascade_validation' => true,
    ));
  }

  public function getBlockPrefix()
  {
    return 'RecipeType';
  }
}
