<?php
namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
  public function load(ObjectManager $manager)
  {
    $categories = array(
      ["name" => "Entrées"],
      ["name" => "Plats"],
      ["name" => "Desserts"],
      ["name" => "Cocktails"],
      ["name" => "Apéritifs"]
    );

    foreach ($categories as $key => $category) {
      $object = new Category();
      $object->setName($category["name"]);
      $manager->persist($object);
    }

    $manager->flush();
  }
}
