<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;


class AdvanceSearchIngredientType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('name', TextType::class, array(
      'label' => "Ingredient",
      'attr' => array(
        'class' => 'ingredient_name_input',
        'placeholder' => 'Nom'
      )
    ))
    ->add('id', HiddenType::class, array(
      'attr' => array(
        'class' => 'ingredient_id_input'
      )
    ))
    ;
  }
}
