<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\Common\Collections\ArrayCollection;

/**
* @ORM\Entity
* @ORM\Table(name="user")
*/
class User extends BaseUser
{
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  protected $id;

  // /**
  // * @ORM\Column(type="bigint", nullable=true)
  // */
  // private $facebook;

  /** @ORM\Column(type="string", length=255, nullable=true) */
  protected $facebookID;

  /** @ORM\Column(type="string", length=255, nullable=true) */
  protected $facebook_access_token;

  /**
  * @ORM\Column(type="string", length=255, nullable=true)
  */
  private $firstname;

  /**
  * @ORM\Column(type="string", length=255, nullable=true)
  */
  private $lastname;

  /**
  * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserHasFavorite", mappedBy="recipe",cascade={"all"},orphanRemoval=true)
  */
  private $favorites;

  /**
  * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserHasHistory", mappedBy="recipe",cascade={"all"},orphanRemoval=true)
  */
  private $histories;


  public function __construct()
  {
    parent::__construct();
    $this->favorites = new ArrayCollection();
    $this->enable = true;
  }

  /**
  * Set username.
  *
  * @param string $username
  *
  * @return User
  */
  public function setUsername($username)
  {
    $this->username = $username;
    $this->email = $username;

    return $this;
  }

  /**
  * Set email.
  *
  * @param string $email
  *
  * @return User
  */
  public function setEmail($email)
  {
    $this->email = $email;
    $this->username = $email;

    return $this;
  }

  /**
  * Set firstname.
  *
  * @param string $firstname
  *
  * @return User
  */
  public function setFirstname($firstname)
  {
    $this->firstname = $firstname;

    return $this;
  }

  /**
  * Get firstname.
  *
  * @return string
  */
  public function getFirstname()
  {
    return $this->firstname;
  }

  /**
  * Set lastname.
  *
  * @param string $lastname
  *
  * @return User
  */
  public function setLastname($lastname)
  {
    $this->lastname = $lastname;

    return $this;
  }

  /**
  * Get lastname.
  *
  * @return string
  */
  public function getLastname()
  {
    return $this->lastname;
  }

    /**
     * Add favorite.
     *
     * @param \AppBundle\Entity\UserHasFavorite $favorite
     *
     * @return User
     */
    public function addFavorite(\AppBundle\Entity\UserHasFavorite $favorite)
    {
        $this->favorites[] = $favorite;
        $favorite->setUser($this);
        return $this;
    }

    /**
     * Remove favorite.
     *
     * @param \AppBundle\Entity\UserHasFavorite $favorite
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFavorite(\AppBundle\Entity\UserHasFavorite $favorite)
    {
        return $this->favorites->removeElement($favorite);
    }

    /**
     * Get favorites.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavorites()
    {
        return $this->favorites;
    }

    /**
     * Add history.
     *
     * @param \AppBundle\Entity\UserHasHistory $history
     *
     * @return User
     */
    public function addHistory(\AppBundle\Entity\UserHasHistory $history)
    {
        $this->histories[] = $history;

        return $this;
    }

    /**
     * Remove history.
     *
     * @param \AppBundle\Entity\UserHasHistory $history
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeHistory(\AppBundle\Entity\UserHasHistory $history)
    {
        return $this->histories->removeElement($history);
    }

    /**
     * Get histories.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHistories()
    {
        return $this->histories;
    }

    /**
     * Set facebookID.
     *
     * @param string|null $facebookID
     *
     * @return User
     */
    public function setFacebookID($facebookID = null)
    {
        $this->facebookID = $facebookID;

        return $this;
    }

    /**
     * Get facebookID.
     *
     * @return string|null
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * Set facebookAccessToken.
     *
     * @param string|null $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken = null)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken.
     *
     * @return string|null
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }
}
