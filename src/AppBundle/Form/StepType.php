<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\TimerType;

class StepType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
    ->add('text', TextType::class, array(
      'label' => false,
      'attr' => array(
        'placeholder' => 'Étape'
      )
    ))
    ->add('timers', CollectionType::class, [
      'entry_type'   => TimerType::class,
      'label'        => 'Timer',
      'allow_add'    => true,
      'allow_delete' => true,
      'prototype'    => true,
      'required'     => false,
      'by_reference' => false,
      'attr'         => [
        'class' => "timers-collection",
      ],
    ])
    ->add('position', HiddenType::class, array(
      'attr' => array(
        'class' => 'step-position'
      )
    ))
    ;
  }

  public function configureOptions(OptionsResolver $resolver)
  {
    $resolver->setDefaults([
      'data_class' => 'AppBundle\Entity\Step',
    ]);
  }

  public function getBlockPrefix()
  {
    return 'StepType';
  }
}
