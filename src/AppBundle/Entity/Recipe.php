<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Sonata\MediaBundle\Entity\Media as Media;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
* @ORM\Entity(repositoryClass="AppBundle\Repository\RecipeRepository")
* @ORM\HasLifecycleCallbacks
*/
class Recipe
{
  /**
  * @ORM\Id
  * @ORM\Column(type="integer")
  * @ORM\GeneratedValue(strategy="AUTO")
  */
  private $id;

  /**
  * @ORM\Column(type="string", length=255, unique=false)
  */
  private $name;

  /**
  * @ORM\Column(type="text", unique=false)
  */
  private $description;

  /**
  * @var ArrayCollection Steps
  * @ORM\OneToMany(targetEntity="AppBundle\Entity\Step", mappedBy="recipe",cascade={"all"},orphanRemoval=true )
  */
  private $steps;

  /**
  * @var ArrayCollection Parts
  * @ORM\OneToMany(targetEntity="AppBundle\Entity\RecipeHasPart", mappedBy="recipe",cascade={"all"},orphanRemoval=true )
  */
  private $parts;

  /**
  * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Equipment")
  */
  private $equipments;

  /**
  * @ORM\Column(type="integer", nullable=true)
  */
  private $cookTime;

  /**
  * @ORM\Column(type="integer")
  */
  private $preparationTime;

  /**
  * @ORM\Column(type="integer", nullable=true)
  */
  private $restTime;

  /**
  * @ORM\Column(type="integer")
  */
  private $forPerson;

  /**
  * @Gedmo\Slug(fields={"name"})
  * @ORM\Column(length=128, unique=true)
  */
  private $slug;

  /**
  * @ORM\ManyToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
  */
  protected $media;

  /**
  * @ORM\Column(type="float", nullable=false)
  */
  private $note;

  /**
  * @var datetime $created_at
  *
  * @ORM\Column(type="datetime")
  */
  protected $created_at;

  /**
  * @var datetime $updated_at
  *
  * @ORM\Column(type="datetime", nullable = true)
  */
  protected $updated_at;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isVegan = false;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isVegetarian = false;

  /**
  * @ORM\Column(type="boolean")
  */
  private $isGlutenfree = false;

  /**
  * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
  */
  private $category;

  // Constructor
  public function __construct()
  {
    $this->note = 3;
    $this->steps = new ArrayCollection();
    $this->equipments = new ArrayCollection();
  }

  /**
  *
  * @ORM\PrePersist
  * @ORM\PreUpdate
  */
  public function updatedTimestamps()
  {
    $this->setUpdatedAt(new \DateTime('now'));

    if ($this->getCreatedAt() == null) {
      $this->setCreatedAt(new \DateTime('now'));
    }

    $isVegan = true;
    $isVegetarian = true;
    $isGlutenfree = true;
    if ($this->parts) {
      foreach ($this->parts as $part) {
        if ($part->getIngredients()) {
          foreach ($part->getIngredients() as $recipe_has_ingredient) {
            if (!$recipe_has_ingredient->getIngredient()->getIsVegan())
            $isVegan = false;
            if (!$recipe_has_ingredient->getIngredient()->getIsVegetarian())
            $isVegetarian = false;
            if (!$recipe_has_ingredient->getIngredient()->getIsGlutenfree())
            $isGlutenfree = false;
          }
        }
      }
    }


    $this->isVegan = $isVegan;
    $this->isVegetarian = $isVegetarian;
    $this->isGlutenfree = $isGlutenfree;
  }

  /**
  * Set Steps
  *
  * @return Recipe
  */
  public function setSteps(ArrayCollection $steps)
  {
    $this->steps = $steps;
    return $this;
  }

  /**
  * Get Steps
  *
  * @return ArrayCollection
  */
  public function getSteps()
  {
    return $this->steps;
  }

  /**
  * Add Step
  *
  * @param Step $step
  * @return $this
  */
  public function addStep(Step $step)
  {
    $step->setRecipe($this);
    $this->steps[] = $step;
    return $this;
  }

  /**
  * Remove Step
  *
  * @param Step $step
  * @return $this
  */
  public function removeStep(Step $step)
  {
    $this->steps->removeElement($step);
    return $this;
  }

  /**
  * Get id
  *
  * @return integer
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set name
  *
  * @param string $name
  *
  * @return Recipe
  */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
  * Get name
  *
  * @return string
  */
  public function getName()
  {
    return $this->name;
  }

  /**
  * Set cookTime
  *
  * @param integer $cookTime
  *
  * @return Recipe
  */
  public function setCookTime($cookTime)
  {
    $this->cookTime = $cookTime;

    return $this;
  }

  /**
  * Get cookTime
  *
  * @return integer
  */
  public function getCookTime()
  {
    return $this->cookTime;
  }

  /**
  * Set preparationTime
  *
  * @param integer $preparationTime
  *
  * @return Recipe
  */
  public function setPreparationTime($preparationTime)
  {
    $this->preparationTime = $preparationTime;

    return $this;
  }

  /**
  * Get preparationTime
  *
  * @return integer
  */
  public function getPreparationTime()
  {
    return $this->preparationTime;
  }

  public function __toString(){
    return $this->getName();
  }

  /**
  * Set restTime
  *
  * @param integer $restTime
  *
  * @return Recipe
  */
  public function setRestTime($restTime)
  {
    $this->restTime = $restTime;

    return $this;
  }

  /**
  * Get restTime
  *
  * @return integer
  */
  public function getRestTime()
  {
    return $this->restTime;
  }

  /**
  * Set forPerson
  *
  * @param integer $forPerson
  *
  * @return Recipe
  */
  public function setForPerson($forPerson)
  {
    $this->forPerson = $forPerson;

    return $this;
  }

  /**
  * Get forPerson
  *
  * @return integer
  */
  public function getForPerson()
  {
    return $this->forPerson;
  }

  /**
  * Get slug
  *
  * @return string
  */
  public function getSlug()
  {
    return $this->slug;
  }

  /**
  * Set description
  *
  * @param string $description
  *
  * @return Recipe
  */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
  * Get description
  *
  * @return string
  */
  public function getDescription()
  {
    return $this->description;
  }

  /**
  * Add part
  *
  * @param \AppBundle\Entity\RecipeHasPart $part
  *
  * @return Recipe
  */
  public function addPart(\AppBundle\Entity\RecipeHasPart $part)
  {
    $this->parts[] = $part;
    $part->setRecipe($this);

    return $this;
  }

  /**
  * Remove part
  *
  * @param \AppBundle\Entity\RecipeHasPart $part
  */
  public function removePart(\AppBundle\Entity\RecipeHasPart $part)
  {
    $this->parts->removeElement($part);
  }

  /**
  * Get parts
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getParts()
  {
    return $this->parts;
  }

  /**
  * @param Media $media
  */
  public function setMedia(Media $media)
  {
    $this->media = $media;
  }

  /**
  * @return Media
  */
  public function getMedia()
  {
    return $this->media;
  }

  /**
  * Set note
  *
  * @param integer $note
  *
  * @return Recipe
  */
  public function setNote($note)
  {
    $this->note = $note;

    return $this;
  }

  /**
  * Get note
  *
  * @return integer
  */
  public function getNote()
  {
    return $this->note;
  }

  /**
  * Set createdAt
  *
  * @param \DateTime $createdAt
  *
  * @return Recipe
  */
  public function setCreatedAt($createdAt)
  {
    $this->created_at = $createdAt;

    return $this;
  }

  /**
  * Get createdAt
  *
  * @return \DateTime
  */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
  * Set updatedAt
  *
  * @param \DateTime $updatedAt
  *
  * @return Recipe
  */
  public function setUpdatedAt($updatedAt)
  {
    $this->updated_at = $updatedAt;

    return $this;
  }

  /**
  * Get updatedAt
  *
  * @return \DateTime
  */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
  * Set isVegan.
  *
  * @param bool $isVegan
  *
  * @return Recipe
  */
  public function setIsVegan($isVegan)
  {
    $this->isVegan = $isVegan;

    return $this;
  }

  /**
  * Get isVegan.
  *
  * @return bool
  */
  public function getIsVegan()
  {
    return $this->isVegan;
  }

  /**
  * Set isVegetarian.
  *
  * @param bool $isVegetarian
  *
  * @return Recipe
  */
  public function setIsVegetarian($isVegetarian)
  {
    $this->isVegetarian = $isVegetarian;

    return $this;
  }

  /**
  * Get isVegetarian.
  *
  * @return bool
  */
  public function getIsVegetarian()
  {
    return $this->isVegetarian;
  }

  /**
  * Set isGlutenfree.
  *
  * @param bool $isGlutenfree
  *
  * @return Recipe
  */
  public function setIsGlutenfree($isGlutenfree)
  {
    $this->isGlutenfree = $isGlutenfree;

    return $this;
  }

  /**
  * Get isGlutenfree.
  *
  * @return bool
  */
  public function getIsGlutenfree()
  {
    return $this->isGlutenfree;
  }

  /**
  * Set category.
  *
  * @param \AppBundle\Entity\Category|null $category
  *
  * @return Recipe
  */
  public function setCategory(\AppBundle\Entity\Category $category = null)
  {
    $this->category = $category;

    return $this;
  }

  /**
  * Get category.
  *
  * @return \AppBundle\Entity\Category|null
  */
  public function getCategory()
  {
    return $this->category;
  }

  /**
  * Set slug.
  *
  * @param string $slug
  *
  * @return Recipe
  */
  public function setSlug($slug)
  {
    $this->slug = $slug;

    return $this;
  }

  /**
  * Add equipment.
  *
  * @param \AppBundle\Entity\Equipment $equipment
  *
  * @return Recipe
  */
  public function addEquipment(\AppBundle\Entity\Equipment $equipment)
  {
    $this->equipments[] = $equipment;

    return $this;
  }

  /**
  * Remove equipment.
  *
  * @param \AppBundle\Entity\Equipment $equipment
  *
  * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
  */
  public function removeEquipment(\AppBundle\Entity\Equipment $equipment)
  {
    return $this->equipments->removeElement($equipment);
  }

  /**
  * Get equipments.
  *
  * @return \Doctrine\Common\Collections\Collection
  */
  public function getEquipments()
  {
    return $this->equipments;
  }
}
